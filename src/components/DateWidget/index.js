import React, { Component } from 'react';
import moment from 'moment-timezone';
import CN from 'classnames';
import './styles.css';

class DateWidget extends Component {
  state = {
    date: moment(),
    dateFormat: this.props.dateFormat || 'DD MM YYYY',
    timezoneFormat: this.props.timezoneFormat || 'Z',
  };

  onToday = () => this.setState({ date: moment() });

  onPrev = () => this.setState(({ date }) => ({date: moment(date).add(-1, 'day')}));

  onNext = () => this.setState(({ date }) => ({date: moment(date).add(1, 'day')}));

  getTimezone = () => {
    const { timezoneFormat } = this.state;
    const name = moment.tz.guess();
    const zone = moment.tz(name).format(timezoneFormat);

    return `${name} ${zone}`;
  };

  render() {
    const { date, dateFormat } = this.state;
    const { from: dateFrom, to: dateTo  } = this.props;
    const isToday = moment().format(dateFormat) === date.format(dateFormat);
    const isPrev = dateFrom.format(dateFormat) === date.format(dateFormat);
    const isNext = dateTo.format(dateFormat) === date.format(dateFormat);

    return (
      <div className="date-container">
        <button
          onClick={ this.onToday }
          disabled={isToday}
          className={isToday ? 'disabled' : ''}
        >
          Today
        </button>
        <button onClick={this.onPrev} disabled={isPrev}>&#10094;</button>
        <p className={CN('date-field', isToday && 'today')}>{date.format(dateFormat)}</p>
        <button onClick={this.onNext} disabled={isNext}>&#10095;</button>
        <p className="zone-field" >{this.getTimezone()}</p>
      </div>
    )
  }
}

export default DateWidget;
